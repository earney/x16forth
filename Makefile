TARGET = cx16

all:
	cl65 -o build/X16FORTH.PRG -t cx16 src/libs/mystring.c src/main.c src/x16Forth.c src/initScript.c

sim:
	cl65 -g -t $(TARGET) src/main.c src/x16Forth.c src/libs/mystring.c -o build/SIM.PRG

run_sim: sim
	sim65 build/SIM.prg