/*******************************************************************************
*
* A minimal Forth compiler in C
* By Leif Bruder <leifbruder@gmail.com> http://defineanswer42.wordpress.com
* Release 2014-04-04
*
* Based on Richard W.M. Jones' excellent Jonesforth sources/tutorial
*
* PUBLIC DOMAIN
*
* I, the copyright holder of this work, hereby release it into the public
* domain. This applies worldwide. In case this is not legally possible, I grant
* any entity the right to use this work for any purpose, without any conditions,
* unless such conditions are required by law.
*
*******************************************************************************/

/* Only a single include here; I'll define everything on the fly to keep
* dependencies as low as possible. In this file, the only C standard functions
* used are getchar, putchar and the EOF value. */
#include <stdio.h>
#include "libs/mystring.h"
#include "x16Forth.h"
//#include "stackops.h"
//#include "builtins.h"
//#include "defs.h"
//#include "misc.h"
//#include "intrepreter.h"
#include "initScript.c"
//#include <cbm.h>
//#include <errno.h>

/* Secure memory access */

//char INPUTSOURCE = 0;    // 0 = keyboard (user input),  1 = file,  2 = test statement.


extern unsigned int filelength;

char openfile(void) {
    printf("reading init file\n");
    filepos=0;
    filelength = mystrlen(filebuffer);
    //printf("file length:%d\n", filelength);
    // INPUTSOURCE = 1;   // file
    return -1;
}

char readfile(void) {
    //printf("reading from file:%c\n", filebuffer[filepos]);
    //if (INPUTSOURCE != 1) {// should produce an error
    //   INPUTSOURCE = 1;  // file
    //}
    if (filepos == filelength) {
        // switch back to user input since we reached the end of file..
    //    INPUTSOURCE = 0;  // user input (keyboard)
        return EOF;
    }
    return filebuffer[filepos++];
}

char closefile(void) {
    filelength=0;
    filebuffer="";
    filelength=0;
    //INPUTSOURCE = 0;  // set input source back to keyboard.. (user input)
    return -1;
}

cell readMem(cell address)
{
    if (address > MEM_SIZE)
    {
        tell("Internal error in readMem: Invalid address\n");
        errorFlag = 1;
        return 0;
    }
    return *((cell*)(memory + address));
}

void writeMem(cell address, cell value)
{
    if (address > MEM_SIZE)
    {
        tell("Internal error in writeMem: Invalid address\n");
        errorFlag = 1;
        return;
    }
    *((cell*)(memory + address)) = value;
}

/* The primary data input function. This is where you place the code to e.g.
* read from a serial line. */
/*
char llkey()
{
    if (*initscript_pos) return *(initscript_pos++);
    if (positionInTestStatement < charsInTestStatement) {
        //putchar(testStatement[positionInTestStatement]);
        return testStatement[positionInTestStatement++];
    }
    return getchar();
}
*/

/* Anything waiting in the keyboard buffer? */
//char keyWaiting()
//{
//    return positionInLineBuffer < charsInLineBuffer  ? -1 : 0;
//}

char BufferNotEmpty()
{
    return (positionInLineBuffer < charsInLineBuffer) || (positionInTestStatement < charsInTestStatement) 
           || (filepos < filelength)  ? -1 : 0;
}

/*
char getfilechar() {
    char c;

    if (positionInLineBuffer < charsInLineBuffer) return lineBuffer[positionInLineBuffer++];  
}
*/

char getCharFromSource() {
    // source 0 = keyboard
    // source 1 = file
    // source 2 = test case

    if (filepos < filelength) return readfile();

    if (positionInTestStatement < charsInTestStatement) {
        //printf("reading from test statement:%c\n", testStatement[positionInTestStatement]);
        return testStatement[positionInTestStatement++];
    }

    //printf("reading from keyboard");
    return getchar();
}

/* Line buffered character input. We're duplicating the functionality of the
* stdio library here to make the code easier to port to other input sources */
char getInput()
{
    char c;

    if (positionInLineBuffer < charsInLineBuffer) {
      //  printf("in key waiting.. %c\n", lineBuffer[positionInLineBuffer]);
        return lineBuffer[positionInLineBuffer++];
    }

    charsInLineBuffer = 0;
    while ((c = getCharFromSource()) != (char) EOF)
    {
        if (charsInLineBuffer == sizeof(lineBuffer)) break;
        lineBuffer[charsInLineBuffer++] = c;
        if (c == '\n') break;
    }

    positionInLineBuffer = 1;
    return lineBuffer[0];
}

byte readWord() {
    char *line = (char*)memory;
    byte len = 0;
    char c;

    while ((c = getInput()) != (char) EOF)
    {
        if (c == ' ') continue;
        if (c == '\n') continue;
        if (c != '\\') break;

        while ((c = getInput()) != (char) EOF)
            if (c == '\n')
                break;
    }

    while (c != ' ' && c != '\n' && c != (char )EOF)
    {
        if (len >= (INPUT_LINE_SIZE - 1))
            break;
        line[++len] = c;
        c = getInput();
    }
    line[0] = len;
    return len;
}

/* Dictionary lookup */
cell findWord(cell address, cell len)
{
    cell ret = *latest;
    char *name = (char*)&memory[address];
    cell i;
    int found;

    for (ret = *latest; ret; ret = readMem(ret))
    {
        if ((memory[ret + CELL_SIZE] & MASK_NAMELENGTH) != len) continue;
        if (memory[ret + CELL_SIZE] & FLAG_HIDDEN) continue;

        found = 1;
        for (i = 0; i < len; i++)
        {
            if (mytoupper(memory[ret + i + 1 + CELL_SIZE]) != mytoupper(name[i]))
            {
                found = 0;
                break;
            }
        }
        if (found) break;
    }
    return ret;
}

/* The basic (data) stack operations */

cell pop()
{
    if (*sp == 1)
    {
        tell("? Stack underflow\n");
        errorFlag = 1;
        return 0;
    }
    return stack[--(*sp)];
}

cell tos()
{
    if (*sp == 1)
    {
        tell("? Stack underflow\n");
        errorFlag = 1;
        return 0;
    }
    return stack[(*sp)-1];
}

void push(cell data)
{
    if (*sp >= STACK_SIZE)
    {
        tell("? Stack overflow\n");
        errorFlag = 1;
        return;
    }
    stack[(*sp)++] = data;
}

dcell dpop()
{
    cell tmp[2];
    tmp[1] = pop();
    tmp[0] = pop();
    return *((dcell*)tmp);
}

void dpush(dcell data)
{
    cell tmp[2];
    *((dcell*)tmp) = data;
    push(tmp[0]);
    push(tmp[1]);
}

// The basic return stack operations

cell rpop()
{
    if (*rsp == 1)
    {
        tell("? RStack underflow\n");
        errorFlag = 1;
        return 0;
    }
    return rstack[--(*rsp)];
}

void rpush(cell data)
{
    if (*rsp >= RSTACK_SIZE)
    {
        tell("? RStack overflow\n");
        errorFlag = 1;
        return;
    }
    rstack[(*rsp)++] = data;
}

void addStatement(const char* statement) {
    //int i;
    //printf("%d\n", strlen(statement));
    mystrcpy(testStatement, statement);
    //for (i=0; i < mystrlen(statement); i++) {
    //    //printf("%s\n", statement[i]);
    //    testStatement[charsInTestStatement++] = statement[i];
    //}
    charsInTestStatement= mystrlen(statement);
    positionInTestStatement=0;
    testStatement[charsInTestStatement++] = '\n';
    testStatement[charsInTestStatement++] = 'b';
    testStatement[charsInTestStatement++] = 'y';
    testStatement[charsInTestStatement++] = 'e';
    testStatement[charsInTestStatement++] = '\n';
}

/* Basic number parsing, base <= 36 only atm */
void parseNumber(byte *word, cell len, dcell *number, cell *notRead, byte *isDouble)
{
    int negative = 0;
    cell i;
    char c;
    cell current;

    *number = 0;
    *isDouble = 0;

    if (len == 0)
    {
        *notRead = 0;
        return;
    }

    if (word[0] == '-')
    {
        negative = 1;
        len--;
        word++;
    }
    else if (word[0] == '+')
    {
        len--;
        word++;
    }

    for (i = 0; i < len; i++)
    {
        c = *word;
        word++;
        if (c == '.') { *isDouble = 1; continue; }
        else if (c >= '0' && c <= '9') current = c - '0';
        else if (c >= 'A' && c <= 'Z') current = 10 + c - 'A';
        else if (c >= 'a' && c <= 'z') current = 10 + c - 'a';
        else break;

        if (current >= *base) break;

        *number = *number * *base + current;
    }

    *notRead = len - i;
    if (negative) *number = (-((scell)*number));
}



/*******************************************************************************
*
* Builtin definitions
*
*******************************************************************************/

cell getCfa(cell address)
{
    byte len = (memory[address + CELL_SIZE] & MASK_NAMELENGTH) + 1;
    while ((len & (CELL_SIZE-1)) != 0) len++;
    return address + CELL_SIZE + len;
}

BUILTIN(0, "RUNDOCOL", docol, 0)
{
    rpush(lastIp);
    next = commandAddress + CELL_SIZE;
}

/* The first few builtins are very simple, not need to waste vertical space here */
BUILTIN( 1, "CELL",      doCellSize,      0)              { push(CELL_SIZE); }
BUILTIN( 2, "@",         memRead,         0)              { push(readMem(pop())); }
BUILTIN( 3, "C@",        memReadByte,     0)              { push(memory[pop()]); }
BUILTIN( 4, "KEY",       key,             0)              { push(getchar()); }
BUILTIN( 5, "EMIT",      emit,            0)              { putkey(pop() & 255); }
BUILTIN( 6, "DROP",      drop,            0)              { pop(); }
BUILTIN( 7, "EXIT",      doExit,          0)              { next = rpop(); }
BUILTIN( 8, "BYE",       bye,             0)              { exitReq = 1; }
BUILTIN( 9, "LATEST",    doLatest,        0)              { push(LATEST_POSITION); }
BUILTIN(10, "HERE",      doHere,          0)              { push(HERE_POSITION); }
BUILTIN(11, "BASE",      doBase,          0)              { push(BASE_POSITION); }
BUILTIN(12, "STATE",     doState,         0)              { push(STATE_POSITION); }
BUILTIN(13, "[",         gotoInterpreter, FLAG_IMMEDIATE) { *state = 0; }
BUILTIN(14, "]",         gotoCompiler,    0)              { *state = 1; }
BUILTIN(15, "HIDE",      hide,            0)              { memory[*latest + CELL_SIZE] ^= FLAG_HIDDEN; }
BUILTIN(16, "R>",        rtos,            0)              { push(rpop()); }
BUILTIN(17, ">R",        stor,            0)              { rpush(pop()); }
BUILTIN(18, "KEY?",      key_p,           0)              { push(BufferNotEmpty()); }
BUILTIN(19, "BRANCH",    branch,          0)              { next += readMem(next); }
BUILTIN(20, "0BRANCH",   zbranch,         0)              { next += pop() ? CELL_SIZE : readMem(next); }
BUILTIN(21, "IMMEDIATE", toggleImmediate, FLAG_IMMEDIATE) { memory[*latest + CELL_SIZE] ^= FLAG_IMMEDIATE; }
BUILTIN(22, "FREE",      doFree,          0)              { push(MEM_SIZE - *here); }
BUILTIN(23, "S0@",       s0_r,            0)              { push(STACK_POSITION + CELL_SIZE); }
BUILTIN(24, "DSP@",      dsp_r,           0)              { push(STACK_POSITION + *sp * CELL_SIZE); }
BUILTIN(25, "NOT",       not,             0)              { push(~pop()); }
BUILTIN(26, "DUP",       dup,             0)              { push(tos()); }

BUILTIN(27, "!", memWrite, 0)
{
    cell address = pop();
    cell value = pop();
    writeMem(address, value);
}

BUILTIN(28, "C!", memWriteByte, 0)
{
    cell address = pop();
    cell value = pop();
    memory[address] = value & 255;
}

BUILTIN(29, "SWAP", swap, 0)
{
    cell a = pop();
    cell b = pop();
    push(a);
    push(b);
}

BUILTIN(30, "OVER", over, 0)
{
    cell a = pop();
    cell b = tos();
    push(a);
    push(b);
}

BUILTIN(31, ",", comma, 0)
{
    push(*here);
    memWrite();
    *here += CELL_SIZE;
}

BUILTIN(32, "C,", commaByte, 0)
{
    push(*here);
    memWriteByte();
    *here += sizeof(byte);
}

BUILTIN(33, "WORD", word, 0)
{
    byte len = readWord();
    push(1);
    push(len);
}

BUILTIN(34, "FIND", find, 0)
{
    cell len = pop();
    cell address = pop();
    cell ret = findWord(address, len);
    push(ret);
}

BUILTIN(35, ">CFA", cfa, 0)
{
    cell address = pop();
    cell ret = getCfa(address);
    if (ret < maxBuiltinAddress)
        push(readMem(ret));
    else
        push(ret);
}

BUILTIN(36, "NUMBER", number, 0)
{
    dcell num;
    cell notRead;
    byte isDouble;
    cell len = pop();
    byte* address = &memory[pop()];
    parseNumber(address, len, &num, &notRead, &isDouble);
    if (isDouble) dpush(num); else push((cell)num);
    push(notRead);
}

BUILTIN(37, "LIT", lit, 0)
{
    push(readMem(next));
    next += CELL_SIZE;
}

// Outer and inner interpreter, TODO split up 
void process_input();
BUILTIN(38, "QUIT", quit, 0)
{
  QUIT_ID = quit_id;  // 38
  process_input();
}

BUILTIN(39, "+", plus, 0)
{
    scell n1 = pop();
    scell n2 = pop();
    push(n1 + n2);
}

BUILTIN(40, "-", minus, 0)
{
    scell n1 = pop();
    scell n2 = pop();
    push(n2 - n1);
}

BUILTIN(41, "*", mul, 0)
{
    scell n1 = pop();
    scell n2 = pop();
    push(n1 * n2);
}

BUILTIN(42, "/MOD", divmod, 0)
{
    scell n1 = pop();
    scell n2 = pop();
    push(n2 % n1);
    push(n2 / n1);
}

BUILTIN(43, "ROT", rot, 0)
{
    cell a = pop();
    cell b = pop();
    cell c = pop();
    push(b);
    push(a);
    push(c);
}

void createWord(const char* name, byte len, byte flags);
BUILTIN(44, "CREATE", doCreate, 0)
{
    byte len;
    cell address;
    word();
    len = pop() & 255;
    address = pop();
    createWord((char*)&memory[address], len, 0);
}

BUILTIN(45, ":", colon, 0)
{
    doCreate();
    push(docol_id);
    comma();
    hide();
    *state = 1;
}

BUILTIN(46, ";", semicolon, FLAG_IMMEDIATE)
{
    push(doExit_id);
    comma();
    hide();
    *state = 0;
}

BUILTIN(47, "R@", rget, 0)
{
    cell tmp = rpop();
    rpush(tmp);
    push(tmp);
}

BUILTIN(48, "J", doJ, 0)
{
    cell tmp1 = rpop();
    cell tmp2 = rpop();
    cell tmp3 = rpop();
    rpush(tmp3);
    rpush(tmp2);
    rpush(tmp1);
    push(tmp3);
}

BUILTIN(49, "'", tick, FLAG_IMMEDIATE)
{
    word();
    find();
    cfa();

    if (*state)
    {
        push(lit_id);
        comma();
        comma();
    }
}

BUILTIN(50, "=", equals, 0)
{
    cell a1 = pop();
    cell a2 = pop();
    push(a2 == a1 ? -1 : 0);
}

BUILTIN(51, "<", smaller, 0)
{
    scell a1 = pop();
    scell a2 = pop();
    push(a2 < a1 ? -1 : 0);
}

BUILTIN(52, ">", larger, 0)
{
    scell a1 = pop();
    scell a2 = pop();
    push(a2 > a1 ? -1 : 0);
}

BUILTIN(53, "AND", doAnd, 0)
{
    cell a1 = pop();
    cell a2 = pop();
    push(a2 & a1);
}

BUILTIN(54, "OR", doOr, 0)
{
    cell a1 = pop();
    cell a2 = pop();
    push(a2 | a1);
}

BUILTIN(55, "?DUP", p_dup, 0)
{
    cell a = tos();
    if (a) push(a);
}

BUILTIN(56, "LITSTRING", litstring, 0)
{
    cell length = readMem(next);
    next += CELL_SIZE;
    push(next);
    push(length);
    next += length;
    while (next & (CELL_SIZE-1))
        next++;
}

BUILTIN(57, "XOR", xor, 0)
{
    cell a = pop();
    cell b = pop();
    push(a ^ b);
}

BUILTIN(58, "*/", timesDivide, 0)
{
    cell n3 = pop();
    dcell n2 = pop();
    dcell n1 = pop();
    dcell r = (n1 * n2) / n3;
    push((cell)r);
    if ((cell)r != r)
    {
        tell("Arithmetic overflow\n");
        errorFlag = 1;
    }
}

BUILTIN(59, "*/MOD", timesDivideMod, 0)
{
    cell n3 = pop();
    dcell n2 = pop();
    dcell n1 = pop();
    dcell r = (n1 * n2) / n3;
    dcell m = (n1 * n2) % n3;
    push((cell)m);
    push((cell)r);
    if ((cell)r != r)
    {
        tell("Arithmetic overflow\n");
        errorFlag = 1;
    }
}

BUILTIN(60, "D=", dequals, 0)
{
    dcell a1 = dpop();
    dcell a2 = dpop();
    push(a2 == a1 ? -1 : 0);
}

BUILTIN(61, "D<", dsmaller, 0)
{
    dscell a1 = dpop();
    dscell a2 = dpop();
    push(a2 < a1 ? -1 : 0);
}

BUILTIN(62, "D>", dlarger, 0)
{
    dscell a1 = dpop();
    dscell a2 = dpop();
    push(a2 > a1 ? -1 : 0);
}

BUILTIN(63, "DU<", dusmaller, 0)
{
    dcell a1 = dpop();
    dcell a2 = dpop();
    push(a2 < a1 ? -1 : 0);
}

BUILTIN(64, "D+", dplus, 0)
{
    dscell n1 = dpop();
    dscell n2 = dpop();
    dpush(n1 + n2);
}

BUILTIN(65, "D-", dminus, 0)
{
    dscell n1 = dpop();
    dscell n2 = dpop();
    dpush(n2 - n1);
}

BUILTIN(66, "D*", dmul, 0)
{
    dscell n1 = dpop();
    dscell n2 = dpop();
    dpush(n1 * n2);
}

BUILTIN(67, "D/", ddiv, 0)
{
    dscell n1 = dpop();
    dscell n2 = dpop();
    dpush(n2 / n1);
}

BUILTIN(68, "2SWAP", dswap, 0)
{
    dcell a = dpop();
    dcell b = dpop();
    dpush(a);
    dpush(b);
}

BUILTIN(69, "2OVER", dover, 0)
{
    dcell a = dpop();
    dcell b = dpop();
    dpush(b);
    dpush(a);
    dpush(b);
}

BUILTIN(70, "2ROT", drot, 0)
{
    dcell a = dpop();
    dcell b = dpop();
    dcell c = dpop();
    dpush(b);
    dpush(a);
    dpush(c);
}

/* Create a word in the dictionary */
void createWord(const char* name, byte len, byte flags)
{
    cell newLatest = *here;
    push(*latest);
    comma();
    push(len | flags);
    commaByte();
    while (len--)
    {
        push(*name);
        commaByte();
        name++;
    }
    while (*here & (CELL_SIZE-1))
    {
        push(0);
        commaByte();
    }
    *latest = newLatest;
}


/* Add a builtin to the dictionary */
void addBuiltin(cell code, const char* name, const byte flags, builtin f)
{
    if (errorFlag) return;

    if (code >= MAX_BUILTIN_ID)
    {
        tell("Error adding builtin ");
        tell(name);
        tell(": Out of builtin IDs\n");
        errorFlag = 1;
        return;
    }

    if (builtins[code] != 0)
    {
        tell("Error adding builtin ");
        tell(name);
        tell(": ID given twice\n");
        errorFlag = 1;
        return;
    }

    builtins[code] = f;
    createWord(name, mystrlen(name), flags);
    push(code);
    comma();
    push(doExit_id);
    comma();
}

void process_input() //(cell quit_id)
{
    cell address;
    dcell number;
    cell notRead;
    cell command;
    int i;
    byte isDouble;
    cell tmp[2];

    int immediate;

    for (exitReq = 0; exitReq == 0;)
    {
        lastIp = next = quit_address;
        errorFlag = 0;

        word();
        find();

        address = pop();
        if (address)
        {
            immediate = (memory[address + CELL_SIZE] & FLAG_IMMEDIATE);
            commandAddress = getCfa(address);
            command = readMem(commandAddress);
            if (*state && !immediate)
            {
                if (command < MAX_BUILTIN_ID && command != docol_id)
                    push(command);
                else
                    push(commandAddress);
                comma();
            }
            else
            {
                while (!errorFlag && !exitReq)
                {
                    if (command == QUIT_ID) break;
                    else if (command < MAX_BUILTIN_ID) builtins[command]();
                    else
                    {
                        lastIp = next;
                        next = command;
                    }

                    commandAddress = next;
                    command = readMem(commandAddress);
                    next += CELL_SIZE;
                }
            }
        }
        else
        {
            parseNumber(&memory[1], memory[0], &number, &notRead, &isDouble);
            if (notRead)
            {
                tell("Unknown word: ");
                for (i=0; i<memory[0]; i++)
                    putkey(memory[i+1]);
                putkey('\n');

                *sp = *rsp = 1;
                continue;
            }
            else
            {
                if (*state)
                {
                    *((dcell*)tmp) = number;
                    push(lit_id);
                    comma();

                    if (isDouble)
                    {
                        push(tmp[0]);
                        comma();
                        push(lit_id);
                        comma();
                        push(tmp[1]);
                        comma();
                    }
                    else
                    {
                        push((cell)number);
                        comma();
                    }
                }
                else
                {
                    if (isDouble) dpush(number); else push((cell)number);
                }
            }
        }

        if (errorFlag)
            *sp = *rsp = 1;
        else if (!BufferNotEmpty())
        //else if (!keyWaiting() && !(*initscript_pos) && (positionInTestStatement >= charsInTestStatement))
           if (exitReq == 0) tell(" OK\n");
    }
}

/* Program setup and jump to outer interpreter */
void init_builtins()
{
    ADD_BUILTIN(docol);
    ADD_BUILTIN(doCellSize);
    ADD_BUILTIN(memRead);
    ADD_BUILTIN(memWrite);
    ADD_BUILTIN(memReadByte);
    ADD_BUILTIN(memWriteByte);
    ADD_BUILTIN(key);
    ADD_BUILTIN(emit);
    ADD_BUILTIN(swap);
    ADD_BUILTIN(dup);
    ADD_BUILTIN(drop);
    ADD_BUILTIN(over);
    ADD_BUILTIN(comma);
    ADD_BUILTIN(commaByte);
    ADD_BUILTIN(word);
    ADD_BUILTIN(find);
    ADD_BUILTIN(cfa);
    ADD_BUILTIN(doExit);
    ADD_BUILTIN(quit);
    quit_address = getCfa(*latest);
    ADD_BUILTIN(number);
    ADD_BUILTIN(bye);
    ADD_BUILTIN(doLatest);
    ADD_BUILTIN(doHere);
    ADD_BUILTIN(doBase);
    ADD_BUILTIN(doState);
    ADD_BUILTIN(plus);
    ADD_BUILTIN(minus);
    ADD_BUILTIN(mul);
    ADD_BUILTIN(divmod);
    ADD_BUILTIN(rot);
    ADD_BUILTIN(gotoInterpreter);
    ADD_BUILTIN(gotoCompiler);
    ADD_BUILTIN(doCreate);
    ADD_BUILTIN(hide);
    ADD_BUILTIN(lit);
    ADD_BUILTIN(colon);
    ADD_BUILTIN(semicolon);
    ADD_BUILTIN(rtos);
    ADD_BUILTIN(stor);
    ADD_BUILTIN(rget);
    ADD_BUILTIN(doJ);
    ADD_BUILTIN(tick);
    ADD_BUILTIN(key_p);
    ADD_BUILTIN(equals);
    ADD_BUILTIN(smaller);
    ADD_BUILTIN(larger);
    ADD_BUILTIN(doAnd);
    ADD_BUILTIN(doOr);
    ADD_BUILTIN(branch);
    ADD_BUILTIN(zbranch);
    ADD_BUILTIN(toggleImmediate);
    ADD_BUILTIN(doFree);
    ADD_BUILTIN(p_dup);
    ADD_BUILTIN(s0_r);
    ADD_BUILTIN(dsp_r);
    ADD_BUILTIN(litstring);
    ADD_BUILTIN(not);
    ADD_BUILTIN(xor);
    ADD_BUILTIN(timesDivide);
    ADD_BUILTIN(timesDivideMod);
    ADD_BUILTIN(dequals);
    ADD_BUILTIN(dsmaller);
    ADD_BUILTIN(dlarger);
    ADD_BUILTIN(dusmaller);
    ADD_BUILTIN(dplus);
    ADD_BUILTIN(dminus);
    ADD_BUILTIN(dmul);
    ADD_BUILTIN(ddiv);
    ADD_BUILTIN(dswap);
    ADD_BUILTIN(dover);
    ADD_BUILTIN(drot);
}

/* Program setup and jump to outer interpreter */
int init()
{
    errorFlag = 0;

    //if (DCELL_SIZE != 2*CELL_SIZE)
    //{
    //    tell("Configuration error: DCELL_SIZE != 2*CELL_SIZE\n");
    //    return 1;
    //}

    state = (cell*)&memory[STATE_POSITION];
    base = (cell*)&memory[BASE_POSITION];
    latest = (cell*)&memory[LATEST_POSITION];
    here = (cell*)&memory[HERE_POSITION];
    sp = (cell*)&memory[STACK_POSITION];
    stack = (cell*)&memory[STACK_POSITION + CELL_SIZE];
    rsp = (cell*)&memory[RSTACK_POSITION];
    rstack = (cell*)&memory[RSTACK_POSITION + CELL_SIZE];

    *sp = *rsp = 1;
    *state = 0;
    *base = 10;
    *latest = 0;
    *here = HERE_START;

    init_builtins();

    maxBuiltinAddress = (*here) - 1;

    if (errorFlag) return 1;
}

cell execute_statement(const char * stmt) {
    addStatement(stmt);
    process_input();
    return tos();
}

/*
void assertEqual(char * stmt, char *value) {
    char * t;
    execute_statement(stmt);
    sprintf(t, "%d", tos());
    //printf("%s", t);
    if (!mystrcmp(value,t)) { 
        printf("%s != %s", t, value);
    } else {
        printf("%s = %s", t, value);
    }
}
*/

void init_words() {
    openfile();
    process_input();
    closefile();
}

/*
int main() {
    init();
    init_words();
    process_input();
    return 0;
}
*/