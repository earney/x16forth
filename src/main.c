//#include <stdio.h>
//#include "x16Forth.h"

extern int init(void);
extern void init_words(void);
extern void process_input(void);

int main(void) {
    init();
    init_words();
    process_input();
    return 0;
}
