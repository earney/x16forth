#ifndef MYSTRING_H
#define MYSTRING_H

void putkey(char c);
void tell(const char *str);
unsigned int mystrlen(const char *str);
void mystrcpy(char * dest, const char *src);
char mytoupper(char c);
char mystrcmp(const char* v1, const char * v2);
#endif