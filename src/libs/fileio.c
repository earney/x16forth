#include <cbm.h>
#include <errno.h>

unsigned int file_begin_addr = x7000;   // change this.
unsigned int file_end_addr = file_begin_addr;

file_open(const char * filname, const * flags) {   // flags is r, w, a (or rw)

   cbm_k_setnam(filename);
   cbm_k_setlfs(2, 8, 0);

   return cbm_k_open();   // opens a logical i/o file
}

unsigned int file_read() {
   const int LOAD 0; 
   file_end_addr=cbm_k_load(LOAD, file_begin_addr);
   return file_end_addr - file_begin_addr;    // return bytes read
}

file_write(data) {
    unsigned int size = mystrlen(data);
    return cbm_k_save(data, data + size);
}

file_close() {
    cbm_k_close(2);
}