#ifndef MYSTRING_H
#define MYSTRING_H

#include <stdio.h>

// simple string functions so we don't have to pull in string.h


/* The primary data output function. This is the place to change if you want
* to e.g. output data on a microcontroller via a serial interface. */
void putkey(char c)
{
    putchar(c);
}

/* C string output */
void tell(const char *str)
{
    //printf("%s", str);
    while (*str) putchar(*str++);
    //    putkey(*str++);
}

unsigned int mystrlen(const char *str)
{
    unsigned int ret = 0;
    while (*str++) ret++;
    return ret;
}

void mystrcpy(char * dest, const char *src) {
    int i;
    for (i=0; i < mystrlen(src); i++) dest[i] = src[i];
}

/* toupper() clone so we don't have to pull in ctype.h */
char mytoupper(char c)
{
    return (c >= 'a' && c <= 'z') ? c - 'a' + 'A' : c;
}

char mystrcmp(char * v1, char * v2) {
    int i;
    int len = mystrlen(v1);
    if (len != mystrlen(v2)) return 0;
    for (i=0; i < len; i++) {
        if (*v1+i != *v2+i) return 0;
    }
    return 1;
}

#endif