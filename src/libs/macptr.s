MACPTR := $FF44

.import popa
.export _macptr ; the _ is important - C symbols all start with _

.proc _macptr: near
    phx
    tax
    jsr popa
    ply
    jsr MACPTR
    bcs macptr_unsupported
macptr_supported:
    txa ; C wants int in .AX not .XY
    phy
    plx
    rts
macptr_unsupported: ; return -1 on error
    ldx #$ff
    lda #$ff
    rts
.endproc 