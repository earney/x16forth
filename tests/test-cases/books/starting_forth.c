#include <stdio.h>

//typedef cell int;
//extern int init(void);
//extern void init_words(void);
extern int execute_statement(const char * stmt);

void sf_chapter2() {
    printf("\nchapter2");
    c64unit_assertIntEqual(5, execute_statement("22 4 /MOD"), "22 4 /MOD != 5");
    c64unit_assertIntEqual(2, execute_statement("."), "MOD != 2");
}

void sf_chapter5() {
    printf("\nchapter5");
    // page 117
    c64unit_assertIntEqual(680, execute_statement("2000 34 100 */"), "2000 34 100 */ != 680");

    // page 118
    c64unit_assertIntEqual(726, execute_statement("227 32 10 */"), "227 32 10 */ != 726");

    execute_statement(": R% 10 */ 5 + 10 / ;");   // add a new word
    c64unit_assertIntEqual(73, execute_statement("227 32 R%"), "227 32 R% != 73");

    // page 119
    c64unit_assertIntEqual(114, execute_statement("171 2 3 */"), "171 2 3 */ != 114");

    // page 122
    execute_statement(": PI DUP * 31416 10000 */ ;");
    c64unit_assertIntEqual(314, execute_statement("10 PI"), "10 PI = 314");   // ??????

    execute_statement(": PI DUP * 355 113 */ ;");
    c64unit_assertIntEqual(314, execute_statement("10 PI"), "10 PI = 314");   // ??????
}

void sf_chapter6() {
    printf("\nchapter 6");

    //execute_statement(": test 3 0 do cr .\" I'm going loopy! \" loop ;");
    //c64unit_assertIntEqual(314, execute_statement("test"), "");   // ??????

    // page 130
    // c64unit_assertCharEqual(("I'm going loopy! \nI'm going loopy! \nI'm going loopy! ", execute_statement("test"),
    //                 "loop not right"));

    // need to add more examples from page 130 that deals with strings

    // page 131
    



}