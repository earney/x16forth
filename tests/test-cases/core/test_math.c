#include <stdio.h>

//typedef cell int;
//extern int init(void);
//extern void init_words(void);
extern int execute_statement(const char * stmt);

void testAdd() {
   printf("\ntestAdd");
   c64unit_assertIntEqual(5, execute_statement("2 3 +"), "2 3 + != 5");
   c64unit_assertIntEqual(1, execute_statement("-2 3 +"), "2 3 + != 1");
   c64unit_assertIntEqual(10, execute_statement("2 3 5 + +"), "2 3 5 + + != 10"); 
}

void testSubtract() {
   c64unit_assertIntEqual(2, execute_statement("5 3 -"), "5 3 - != 2"); 
   c64unit_assertIntEqual(-8, execute_statement("-5 3 -"), "5 3 - != -8"); 
}

void testMultiply() {
   c64unit_assertIntEqual(6, execute_statement("2 3 *"), "2 3 * != 6");
}

void testDivide() {
   c64unit_assertIntEqual(2, execute_statement("6 3 /"), "6 3 / != 2");
   c64unit_assertIntEqual(-2, execute_statement("-6 3 /"), "6 3 / != -2");
   c64unit_assertIntEqual(1, execute_statement("6 4 /"), "6 4 / != 1");
   
}

void testMod() {
   c64unit_assertIntEqual(0, execute_statement("6 3 MOD"), "6 3 MOD != 0");
   c64unit_assertIntEqual(2, execute_statement("6 4 MOD"), "6 4 MOD != 2");   
}