
void printProgressIndicator() {
	c64unit_poke(C64UNIT_SCREEN + c64unit_progressIndicator - 1, 0x2E); // dot character
}

void incrementProgressIndicator() {
	++c64unit_progressIndicator;
	printProgressIndicator();
}

int countIndicatorRows() {
	return c64unit_progressIndicator / 40;
}
