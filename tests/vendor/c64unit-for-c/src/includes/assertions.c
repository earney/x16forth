
static const char * ASSERT_EQUAL_FAILED_MESSAGE = "assert equal failed.";
static const char * ASSERT_FALSE_FAILED_MESSAGE = "assert false failed.";
static const char * ASSERT_GREATER_FAILED_MESSAGE = "assert greater failed.";
static const char * ASSERT_GREATER_OR_EQUAL_FAILED_MESSAGE = "assert greater or equal failed.";
static const char * ASSERT_NOT_EQUAL_FAILED_MESSAGE = "assert not equal failed.";
static const char * ASSERT_TRUE_FAILED_MESSAGE = "assert true failed.";

void c64unit_assertCharEqual(char * expected, char * actual, char * customMessage) {
	if (!strcmp(expected, actual)) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_EQUAL_FAILED_MESSAGE);
	displayAssertCharDetailsMessage(expected, actual);
	assertionFailed();
}

void c64unit_assertCharNotEqual(char * expected, char * actual, char * customMessage) {
	if (strcmp(expected, actual)) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_NOT_EQUAL_FAILED_MESSAGE);
	displayAssertCharDetailsMessage(expected, actual);
	assertionFailed();
}

void c64unit_assertFalse(bool actual, char * customMessage) {
	if (actual == false) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_FALSE_FAILED_MESSAGE);
	displayAssertCharDetailsMessage("false", "true");
	assertionFailed();
}

void c64unit_assertGreater(int expected, int actual, char * customMessage) {
	if (expected > actual) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_GREATER_FAILED_MESSAGE);
	displayAssertIntDetailsMessage(expected, actual);
	assertionFailed();
}

void c64unit_assertGreaterOrEqual(int expected, int actual, char * customMessage) {
	if (expected >= actual) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_GREATER_OR_EQUAL_FAILED_MESSAGE);
	displayAssertIntDetailsMessage(expected, actual);
	assertionFailed();
}

void c64unit_assertIntEqual(int expected, int actual, char * customMessage) {
	if (expected == actual) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_EQUAL_FAILED_MESSAGE);
	displayAssertIntDetailsMessage(expected, actual);
	assertionFailed();
}

void c64unit_assertIntNotEqual(int expected, int actual, char * customMessage) {
	if (expected != actual) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_NOT_EQUAL_FAILED_MESSAGE);
	displayAssertIntDetailsMessage(expected, actual);
	assertionFailed();
}

void c64unit_assertTrue(bool actual, char * customMessage) {
	if (actual == true) {
		incrementProgressIndicator();
		return;
	}
	displayCustomMessage(customMessage);
	displayAssertMessage(ASSERT_TRUE_FAILED_MESSAGE);
	displayAssertCharDetailsMessage("true", "false");
	assertionFailed();
}
