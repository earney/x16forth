
static const char * ALL_TESTS_PASSED_MESSAGE = "all tests passed.";
static const char * NUMBER_OF_ASSERTIONS_MESSAGE = "number of assertions: ";

void displayHappyEndMessage() {
	gotoxy(0, 23);
	revers(1);
	printf(ALL_TESTS_PASSED_MESSAGE);

	gotoxy(0, 24);
	revers(0);
	printf(NUMBER_OF_ASSERTIONS_MESSAGE);
	printf("%d", c64unit_progressIndicator);
}
