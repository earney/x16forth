
void termination(int exitStatus) {
	if (!c64unit_exitToBasicMode) {
		for(;;);
	}
	gotoy(countIndicatorRows());
	exit(exitStatus);
}

void assertionFailed() {
	bordercolor(2);
	termination(EXIT_FAILURE);
}

void c64unit(bool exitToBasicModeFlag) {
	c64unit_exitToBasicMode = exitToBasicModeFlag;
	clrscr();
	textcolor(14);
	bgcolor(11);
	bordercolor(12);
}

void c64unit_exit() {
	bordercolor(5);
	displayHappyEndMessage();
	termination(EXIT_SUCCESS);
}
