
#ifndef C64UNIT_H_INCLUDED
#define C64UNIT_H_INCLUDED

#include <stdbool.h>

void c64unit(bool exitToBasicMode);
int c64unit_exit();
void c64unit_assertCharEqual(char * expected, char * actual, char * customMessage);
void c64unit_assertCharNotEqual(char * expected, char * actual, char * customMessage);
void c64unit_assertFalse(bool actual, char * customMessage);
void c64unit_assertGreater(int expected, int actual, char * customMessage);
void c64unit_assertGreaterOrEqual(int expected, int actual, char * customMessage);
void c64unit_assertIntEqual(int expected, int actual, char * customMessage);
void c64unit_assertIntNotEqual(int expected, int actual, char * customMessage);
void c64unit_assertTrue(bool actual, char * customMessage);

#endif
