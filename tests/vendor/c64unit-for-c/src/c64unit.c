
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <stdbool.h>

// Macro to poke value to given address
#define c64unit_poke(addr, val) (*(unsigned char*) (addr) = (val))


// Variables
int c64unit_dataSet = 1;
int c64unit_dataSetLength = 0;
bool c64unit_exitToBasicMode = true;
int c64unit_progressIndicator = 0;


// Constants
const int C64UNIT_SCREEN = 0x0400;


// Includes
#include "includes/progress-indicator.c"
#include "includes/messages.c"
#include "includes/core.c"
#include "includes/display.c"
#include "includes/assertions.c"
