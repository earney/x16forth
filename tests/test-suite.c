#include <c64unit.h>

// source code to be unit tested
// #include "./../src/green-function.c"

//test cases
#include "test-cases/books/starting_forth.c"

#include "test-cases/core/test_builtins.c"
#include "test-cases/core/test_math.c"

extern int init(void);
extern void init_words(void);
extern int execute_statement(const char * stmt);


int main(void) {
    c64unit(true);

    init();
    init_words();
    
    // Examine test cases
    testAdd();
    testSubtract();
    testMultiply();
    testDivide();
    testMod();

    // test books
    printf("\n\nbooks..");
    printf("\nstarting forth");
    sf_chapter2();
    sf_chapter5();

    return c64unit_exit();
}
